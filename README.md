# Separation of 3rivers-monolith into Rock

3Rivers API endpoints

```mermaid
graph TB
    subgraph Rock["Intention to move to 3Rivers Rock(BX)"]
        APIBuyerBankAccount["BuyerBankAccount"]
        APIBuyerCode["BuyerCode"]
        APIBuyer["Buyer"]
        APIBuyerApplyFormValidation["BuyerApplyFormValidation"]
        APIBuyerApplication["BuyerApplication"]
        APIBuyerInvitation["BuyerInvitation"]
        APICustomerBankAccount["CustomerBankAccount"]
        APICustomer["Customer"]
        APIBuyerPayment["BuyerPayment?"]
        APICustomerPayment["CustomerPayment?"]
        APIBuyerContacts["BuyerContacts"]
        APICreateBuyerUser["CreateBuyerUser"]
        subgraph B2bcInboundWebhookForBuyer["Inbound B2bc webhook"]
            buyer.updated
            buyer_payment.created
            buyer_payment.updated
        end
    end
    APIAppAlert["AppAlert"]
    APIChargeCredit["ChargeCredit"]
    APIChargeDispute["ChargeDispute"]
    APITapInboundWebhook["TapInboundWebhook"]
    APIInvoice["Invoice"]
    APIPreauthorization["Preauthorization"]
    APIProgramAgreement["ProgramAgreement"]
    APIProgram["Program"]
    APIProgramPublicKey["ProgramPublicKey"]
    APIPaymentTerms["PaymentTerms"]
    APITermsAndConditions["TermsAndConditions"]
    APISeller["Seller"]
    APISSO["SSO"]
    APIWebhook["Webhook"]
    subgraph B2bcInboundWebhookRemaining["Inbound B2bc webhook"]
        invoice.created
        invoice.updated
        preauthorization.created
        preauthorization.created
    end
    APIUser["Generic User"]

    style Rock fill: #1c54d9
```

3Rivers GraphQL endpoints
```mermaid
graph TB
    subgraph Rock["Intention to move to 3Rivers Rock(BX)"]
        GQLBuyerBankAccount["BuyerBankAccount"]
        GQLBuyerPurchasePolicy["BuyerPurchasePolicy"]
        GQLBuyerUser["BuyerUser"]
        GQLBuyer["Buyer"]
        GQLCustomer["Customer"]
        GQLCustomerGroup["CustomerGroup"]
        GQLBuyerPayment["BuyerPayment?"]
        GQLPaymentApply["PaymentApply?"]
        GQLPaymentInfo["PaymentInfo?"]
    end
    GQLApiKey["ApiKey"]
    GQLAppAlert["AppAlert"]
    GQLChargeActivity["ChargeActivity"]
    GQLCharge["Charge"]
    GQLClient["Client"]
    GQLCountry["Country"]
    GQLDisbursement["Disbursement"]
    GQLDispute["Dispute"]
    GQLInvoice["Invoice"]
    GQLLocale["Locale"]
    GQLMstsSupport["MstsSupport"]
    GQLPreauthorization["Preauthorization"]
    GQLProgramQualification["ProgramQualification"]
    GQLSeller["Seller"]
    GQLTermsConsent["TermsConsent"]
    GQLUser["User"]
    GQLUserMfa["UserMfa"]
    GQLUserNoficiation["UserNoficiation"]

    style Rock fill: #1c54d9
```
